const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const multer = require('multer');

dotenv.config()

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});

const upload = multer({ storage: storage });

const studentSchema = new mongoose.Schema({
    nama: {
        type: String,
        required: true,
    },
    npm: {
        type: String,
        required: true,
        unique: true,    
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    tanggal_dibuat: {
        type: Date,
        default: new Date(),
    }
})

const imageSchema = new mongoose.Schema({
    img:
    {
        data: Buffer,
        contentType: String
    }
});

const Student = mongoose.model('studentdata',studentSchema);
const Image = mongoose.model('imagedata',imageSchema);

const MONGO_PASS = process.env.MONGO_PASS

const app = express();
app.use(bodyParser.urlencoded({ extended: true  }));
app.use(bodyParser.json());

const url = `mongodb+srv://kefassatrio:${MONGO_PASS}@cluster0.hwob7.mongodb.net/lab4?retryWrites=true&w=majority`;
mongoose.connect(url,{useNewUrlParser: true});
const con= mongoose.connection;
app.use(express.json());
try{
    con.on('open',() => {
        console.log('connected');
    })
}catch(error)
{
    console.log("Error: "+error);
}

const port = 34972;

app.get('/mahasiswa/:npm', (req, res) => {
    const npm = req.params.npm;
    (async () => {
        const student = await Student.findOne({ npm: npm });
        res.json(student);
    })();
})
  
app.post('/mahasiswa', (req, res) => {
    console.log(req.body);
    const student = new Student(req.body);
    (async () => {
        await student.save();
        res.json(student)
    })();
})

app.put('/mahasiswa/:npm', (req, res) => {
    const npm = req.params.npm;
    const { nama, email } = req.body;
    (async () => {
        const student = await Student.findOne({ npm: npm });
        student.nama = nama;
        student.email = email;
        await student.save();
        res.json(student);
    })();
})

app.delete('/mahasiswa/:npm', (req, res) => {
    const npm = req.params.npm;
    (async () => {
        const student = await Student.deleteOne({ npm: npm });
        res.json(student);
    })();
})

app.get('/image', (req, res) => {
    Image.find({}, (err, items) => {
        if (err) {
            console.log(err);
            res.status(500).send('An error occurred', err);
        }
        else {
            res.json(items)
        }
    });
});

app.post('/image', upload.single('image'), (req, res, next) => {
    console.log(req.file)
    var obj = {
        img: {
            data: fs.readFileSync(path.join(__dirname + '/uploads/' + req.file.filename)),
            contentType: 'image/png'
        }
    }
    Image.create(obj, (err, item) => {
        if (err) {
            console.log(err);
        }
        else {
            item.save();
            res.json(item)
        }
    });
});

app.listen(port, () =>{
    console.log('Server started');
})
